#!/bin/bash
  
[[ -z $1 ]] && arg=127.0.0.1 || arg=$1

sed -i "s/CHANGEME/$arg/g" docker-volume/html/Src/Configs/database.php
chown -R www-data  docker-volume/html

exit 0